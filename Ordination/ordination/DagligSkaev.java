package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private final ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	public Dosis opretDosis(LocalTime tid, double antal) {
		if (antal < 1) {
			throw new IllegalArgumentException("Antal skal være over 0");
		}
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
		return dosis;
	}

	@Override
	public double samletDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		return sum * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "Daglig skæv";

	}
}

package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> datoer = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, patient, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Returner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(this.getSlutDen()) && givesDen.isAfter(this.getStartDen())
				|| givesDen.isEqual(getSlutDen()) || givesDen.isEqual(getStartDen())) {
			datoer.add(givesDen);
			return true;
		}

		return false;
	}

	public double doegnDosis() {
		LocalDate first = LocalDate.MAX;
		LocalDate last = LocalDate.MIN;

		for (LocalDate d : datoer) {
			if (d.isBefore(first)) {
				first = d;
			} 
			if (d.isAfter(last)) {
				last = d;
			}
		}

		int dageImellem = (int) ChronoUnit.DAYS.between(first, last) + 1;
		System.out.println(dageImellem);

		return (datoer.size() * antalEnheder) / dageImellem;
	}

	public double samletDosis() {
		return datoer.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoer.size();
	}

	@Override
	public String getType() {
		return "Pro necessare";
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public List<LocalDate> getDatoer() {
		return new ArrayList<LocalDate>(datoer);
	}
}

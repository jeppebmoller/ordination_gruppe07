package ordination;

import java.util.ArrayList;
import java.util.List;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
	private List<Ordination> ordinationer = new ArrayList<>();

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
	}

	/**
	 * Metoder til at vedligeholde linkattribut
	 * @return kopi af liste med ordinationer
	 */
	public List<Ordination> getOrdinationer() {
		return new ArrayList<>(ordinationer);
	}

	/**
	 * Tilføjer ordiantion til listen, hvis den ikke allerede er der
	 * @param o
	 */
	public void addOrdination(Ordination o) {
		if(!ordinationer.contains(o)) {
			ordinationer.add(o);
		}
	}
	
	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

}

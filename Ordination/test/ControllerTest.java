package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {
	private Controller controller;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getTestController();
		patient = controller.opretPatient("190997-0535", "Børge Pedersen", 80);
		laegemiddel = controller.opretLaegemiddel("Panodil", 1, 2, 3, "stk");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNOrdination() {
		// Test case 1:
		PN pn = controller.opretPNOrdination(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 10), patient,
				laegemiddel, 2);
		assertEquals(LocalDate.of(2020, 01, 01), pn.getStartDen());
		assertEquals(LocalDate.of(2020, 01, 10), pn.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(pn));
		assertEquals(laegemiddel, pn.getLaegemiddel());
		assertEquals(2, pn.getAntalEnheder(), 0.001);

		// Test case 2: Fejler, da startdato er efter slutdato
		PN pn1 = controller.opretPNOrdination(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 05), patient,
				laegemiddel, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination() {
		// Test case 1:
		DagligFast df1 = controller.opretDagligFastOrdination(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 10),
				patient, laegemiddel, 1, 2, 1, 4);
		assertEquals(LocalDate.of(2020, 01, 01), df1.getStartDen());
		assertEquals(LocalDate.of(2020, 01, 10), df1.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(df1));
		assertEquals(laegemiddel, df1.getLaegemiddel());
		assertEquals(1, df1.getDoser()[0].getAntal(), 0.001);
		assertEquals(2, df1.getDoser()[1].getAntal(), 0.001);
		assertEquals(1, df1.getDoser()[2].getAntal(), 0.001);
		assertEquals(4, df1.getDoser()[3].getAntal(), 0.001);

		// Test case 2:
		DagligFast df2 = controller.opretDagligFastOrdination(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 10),
				patient, laegemiddel, 1, 0, 1, 4);
		assertEquals(LocalDate.of(2020, 01, 01), df2.getStartDen());
		assertEquals(LocalDate.of(2020, 01, 10), df2.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(df2));
		assertEquals(laegemiddel, df2.getLaegemiddel());
		assertEquals(1, df2.getDoser()[0].getAntal(), 0.001);
		assertEquals(0, df2.getDoser()[1].getAntal(), 0.001);
		assertEquals(1, df2.getDoser()[2].getAntal(), 0.001);
		assertEquals(4, df2.getDoser()[3].getAntal(), 0.001);

		// Test case 3: Fejler, da startdat0 er efter slutdato
		DagligFast df3 = controller.opretDagligFastOrdination(LocalDate.of(2020, 01, 10), LocalDate.of(2020, 01, 01),
				patient, laegemiddel, 1, 0, 1, 4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination() {
		// Test case 1:
		LocalTime[] klokkeslæt1 = { LocalTime.of(10, 00), LocalTime.of(14, 15), LocalTime.of(19, 00) };
		double[] antalEnheder1 = { 1.7, 2.8, 4.9 };
		DagligSkaev dagligSkaev1 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 03, 10),
				LocalDate.of(2020, 03, 20), patient, laegemiddel, klokkeslæt1, antalEnheder1);

		assertEquals(LocalDate.of(2020, 03, 10), dagligSkaev1.getStartDen());
		assertEquals(LocalDate.of(2020, 03, 20), dagligSkaev1.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(dagligSkaev1));
		assertEquals(laegemiddel, dagligSkaev1.getLaegemiddel());

		// Fejl: Test case 2:
		// Fejler fordi startDatoen er efter slutDatoen, og der kastes en exception.
		LocalTime[] klokkeslæt2 = { LocalTime.of(13, 00), LocalTime.of(16, 15), LocalTime.of(17, 03) };
		double[] antalEnheder2 = { 2.7, 3.2, 5.6 };
		DagligSkaev dagligSkaev2 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 02, 10), patient, laegemiddel, klokkeslæt2, antalEnheder2);

		// Fejl: Test case 3:
		// Fejler fordi klokkeslæt3 arrayet og antalEnheder3 arrayet ikke er lige lange,
		// og der kastes en exception.
		LocalTime[] klokkeslæt3 = { LocalTime.of(14, 00), LocalTime.of(16, 00), LocalTime.of(17, 00) };
		double[] antalEnheder3 = { 2.1, 3.9 };
		DagligSkaev dagligSkaev3 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 10, 10),
				LocalDate.of(2020, 10, 20), patient, laegemiddel, klokkeslæt3, antalEnheder3);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testOrdinationPNAnvendt() {
		// Test case 1:
		PN pn1 = controller.opretPNOrdination(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 20), patient,
				laegemiddel, 5.9);
		controller.ordinationPNAnvendt(pn1, LocalDate.of(2020, 02, 15));

		assertNotNull(pn1);

		// Fejl: Test case 2:
		// Fejler, fordi datoen ikke er indenfor gyldighedsperioden, og der kastes en
		// exception.
		PN pn2 = controller.opretPNOrdination(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 20), patient,
				laegemiddel, 5.9);
		controller.ordinationPNAnvendt(pn2, LocalDate.of(2020, 03, 10));

		assertNotNull(pn2);

	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		// Test case 1:
		Patient p1 = new Patient("123456-7890", "Søren Nielsen", 20);
		assertEquals(20, controller.anbefaletDosisPrDoegn(p1, laegemiddel), 0.001);

		// Test case 2:
		Patient p2 = new Patient("234567-8901", "Ida Iversen", 50);
		assertEquals(100, controller.anbefaletDosisPrDoegn(p2, laegemiddel), 0.001);

		// Test case 3:
		Patient p3 = new Patient("345678-9012", "Jørgen Jørgensen", 125);
		assertEquals(375, controller.anbefaletDosisPrDoegn(p3, laegemiddel), 0.001);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		// Test case 1:
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 14), patient, laegemiddel);
		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(70, 90, laegemiddel));

		// Test case 2:
		assertEquals(0, controller.antalOrdinationerPrVægtPrLægemiddel(90, 110, laegemiddel));
	}

	@Test
	public void testOpretPatient() {
		// Test case 1:
		Patient p = controller.opretPatient("190997-0535", "Børge Pedersen", 80);
		assertEquals("190997-0535", p.getCprnr());
		assertEquals("Børge Pedersen", p.getNavn());
		assertEquals(80, p.getVaegt(), 0.001);
		assertTrue(controller.getStorage().getAllPatienter().contains(p));
	}

	@Test
	public void testOpretLaegemiddel() {
		// Test case 1:
		Laegemiddel l = controller.opretLaegemiddel("Panodil", 1, 2, 3, "stk");
		assertEquals("Panodil", l.getNavn());
		assertEquals(1, l.getEnhedPrKgPrDoegnLet(), 0.001);
		assertEquals(2, l.getEnhedPrKgPrDoegnNormal(), 0.001);
		assertEquals(3, l.getEnhedPrKgPrDoegnTung(), 0.001);
		assertEquals("stk", l.getEnhed());
	}

}

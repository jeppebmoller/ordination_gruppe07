package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligFastTest {
	private Patient patient;
	private Laegemiddel laegemiddel;
	private DagligFast dagligFast;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("190997-0535", "Børge Pedersen", 80);
		laegemiddel = new Laegemiddel("Panodil", 1, 2, 3, "stk");
		dagligFast = new DagligFast(LocalDate.of(2020, 05, 05), LocalDate.of(2020, 05, 20), patient, laegemiddel, 0, 0,
				0, 0);
	}

	@Test
	public void testSamletDosis() {
		// Test case 1:
		DagligFast df1 = new DagligFast(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 10), patient, laegemiddel, 1,
				2, 3, 4);
		assertEquals(10, df1.samletDosis(), 0.001);

		// Test case 2:
		DagligFast df2 = new DagligFast(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 10), patient, laegemiddel, 1,
				0, 3, 4);
		assertEquals(8, df2.samletDosis(), 0.001);

		// Test case 3:
		DagligFast df3 = new DagligFast(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 14), patient, laegemiddel, 1,
				2, 3, 4);
		assertEquals(50, df3.samletDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis() {
		// Test case 1:
		DagligFast df1 = new DagligFast(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 10), patient, laegemiddel, 1,
				2, 3, 4);
		assertEquals(10, df1.doegnDosis(), 0.001);

		// Test case 2:
		DagligFast df2 = new DagligFast(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 14), patient, laegemiddel, 2,
				2, 2, 2);
		assertEquals(8, df2.doegnDosis(), 0.001);
	}

	@Test
	public void testDagligFast() {
		// Test case 1:
		DagligFast dagligFast = new DagligFast(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 19), patient,
				laegemiddel, 1, 2, 3, 4);

		assertEquals(LocalDate.of(2020, 02, 10), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2020, 02, 19), dagligFast.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(dagligFast));
		assertEquals(laegemiddel, dagligFast.getLaegemiddel());
		assertEquals(1, dagligFast.getDoser()[0].getAntal(), 0.001);
		assertEquals(2, dagligFast.getDoser()[1].getAntal(), 0.001);
		assertEquals(3, dagligFast.getDoser()[2].getAntal(), 0.001);
		assertEquals(4, dagligFast.getDoser()[3].getAntal(), 0.001);
	}

	@Test
	public void testCreateDosis() {
		// Test case 1: Vi tester her, at værdierne på de tomme pladser, ikke er blevet
		// initialiseret
		dagligFast.createDosis(LocalTime.of(23, 59), 2);
		assertEquals(2, dagligFast.getDoser()[2].getAntal(), 0.001);

		assertEquals(0, dagligFast.getDoser()[0].getAntal(), 0.001);
		assertEquals(0, dagligFast.getDoser()[1].getAntal(), 0.001);
		assertEquals(0, dagligFast.getDoser()[3].getAntal(), 0.001);

		// Test case 2:
		dagligFast.createDosis(LocalTime.of(00, 00), 2);
		assertEquals(2, dagligFast.getDoser()[3].getAntal(), 0.001);

		// Test case 3:
		dagligFast.createDosis(LocalTime.of(05, 59), 2);
		assertEquals(2, dagligFast.getDoser()[3].getAntal(), 0.001);

		// Test case 4;:
		dagligFast.createDosis(LocalTime.of(06, 00), 2);
		assertEquals(2, dagligFast.getDoser()[0].getAntal(), 0.001);

		// Test case 5:
		dagligFast.createDosis(LocalTime.of(11, 59), 2);
		assertEquals(2, dagligFast.getDoser()[0].getAntal(), 0.001);

		// Test case 6:
		dagligFast.createDosis(LocalTime.of(12, 00), 2);
		assertEquals(2, dagligFast.getDoser()[1].getAntal(), 0.001);

		// Test case 7:
		dagligFast.createDosis(LocalTime.of(17, 59), 2);
		assertEquals(2, dagligFast.getDoser()[1].getAntal(), 0.001);

		// Test case 8:
		dagligFast.createDosis(LocalTime.of(18, 00), 2);
		assertEquals(2, dagligFast.getDoser()[2].getAntal(), 0.001);

		// Test case 9:
		dagligFast.createDosis(LocalTime.of(07, 00), 8);
		assertEquals(8, dagligFast.getDoser()[0].getAntal(), 0.001);
	}

}

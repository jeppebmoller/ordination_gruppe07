package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {
	private Patient patient;
	private Laegemiddel laegemiddel;
	private DagligSkaev dagligSkaev;

	@Before
	public void setUp() {
		patient = new Patient("190997-0535", "Børge Pedersen", 80);
		laegemiddel = new Laegemiddel("Panodil", 1, 2, 3, "stk");
		dagligSkaev = new DagligSkaev(LocalDate.of(2020, 02, 01), LocalDate.of(2020, 02, 05), patient, laegemiddel);
	}

	@Test
	public void testSamletDosis() {
		// Test case 1:
		dagligSkaev.opretDosis(LocalTime.of(10, 30), 2);
		dagligSkaev.opretDosis(LocalTime.of(12, 00), 3);
		assertEquals(25, dagligSkaev.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		// Test case 1:
		dagligSkaev.opretDosis(LocalTime.of(10, 30), 2);
		dagligSkaev.opretDosis(LocalTime.of(12, 00), 3);
		assertEquals(5, dagligSkaev.doegnDosis(), 0.001);
	}

	@Test
	public void testDagligSkaev() {
		// Test case 1:
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 02, 01), LocalDate.of(2020, 02, 05), patient, laegemiddel);
		assertEquals(LocalDate.of(2020, 02, 01), ds.getStartDen());
		assertEquals(LocalDate.of(2020, 02, 05), ds.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(ds));
		assertEquals(laegemiddel, ds.getLaegemiddel());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDosis() {
		// Test case 1:
		Dosis dosis = dagligSkaev.opretDosis(LocalTime.of(10, 30), 2);
		assertEquals(LocalTime.of(10, 30), dosis.getTid());
		assertEquals(2, dosis.getAntal(), 0.001);
		assertTrue(dagligSkaev.getDoser().contains(dosis));

		// Test case 2: Fejler og kaster exception
		Dosis d = dagligSkaev.opretDosis(LocalTime.of(12, 00), 0);
	}

}

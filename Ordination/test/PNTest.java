package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	private Patient patient;
	private Laegemiddel laegemiddel;
	private PN pn;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("190997-0535", "Børge Pedersen", 80);
		laegemiddel = new Laegemiddel("Panodil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 10), patient, laegemiddel, 5);
	}

	@Test
	public void testSamletDosis() {
		// Test case 1:
		PN p1 = new PN(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 10), patient, laegemiddel, 5);
		p1.givDosis(LocalDate.of(2020, 02, 10));
		assertEquals(5, p1.samletDosis(), 0.001);

		// Test case 2:
		PN p2 = new PN(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 14), patient, laegemiddel, 5);
		p2.givDosis(LocalDate.of(2020, 02, 11));
		p2.givDosis(LocalDate.of(2020, 02, 12));
		assertEquals(10, p2.samletDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis() {
		// Test case 1:
		PN p1 = new PN(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 10), patient, laegemiddel, 5);
		p1.givDosis(LocalDate.of(2020, 02, 10));
		assertEquals(5, p1.doegnDosis(), 0.001);

		// Test case 2:
		PN p2 = new PN(LocalDate.of(2020, 02, 10), LocalDate.of(2020, 02, 14), patient, laegemiddel, 5);
		p2.givDosis(LocalDate.of(2020, 02, 11));
		p2.givDosis(LocalDate.of(2020, 02, 12));
		p2.givDosis(LocalDate.of(2020, 02, 13));
		assertEquals(5, p2.doegnDosis(), 0.001);
	}

	@Test
	public void testPN() {
		// Test case 1:
		PN p1 = new PN(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 10), patient, laegemiddel, 5);
		assertEquals(LocalDate.of(2020, 01, 01), p1.getStartDen());
		assertEquals(LocalDate.of(2020, 01, 10), p1.getSlutDen());
		assertTrue(patient.getOrdinationer().contains(p1));
		assertEquals(laegemiddel, p1.getLaegemiddel());
		assertEquals(5, p1.getAntalEnheder(), 0.001);
	}

	@Test
	public void testGivDosis() {
		// Test case 1:
		boolean test1 = pn.givDosis(LocalDate.of(2020, 01, 05));
		assertTrue(test1);
		assertTrue(pn.getDatoer().contains(LocalDate.of(2020, 01, 05)));

		// Test case 2:
		boolean test2 = pn.givDosis(LocalDate.of(2020, 01, 10));
		assertTrue(test2);
		assertTrue(pn.getDatoer().contains(LocalDate.of(2020, 01, 10)));

		// Test case 3:
		boolean test3 = pn.givDosis(LocalDate.of(2020, 10, 10));
		assertFalse(test3);
		assertFalse(pn.getDatoer().contains(LocalDate.of(2020, 10, 10)));
	}

}
